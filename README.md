# OZIPersistenceDemo

#### 介绍
自定义持久层框架Demo，此Demo仅为个人学习编写，目前实现了单条查询、所有查询以及单条的增删改

#### JDBC问题分析及解决思路

```Java
public void normalJdbc() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            //加载数据库驱动
            Class.forName("com.mysql.jdbc.Driver");
            //获取数据库链接
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ozdemo?characterEncoding = utf - 8", " root", " 123456");
            // 定义sql ?代表占位符
            String sql = "select * from user where username = ?";
            // 获取预处理Statement
            preparedStatement = connection.prepareStatement(sql);
            //设置参数 第一个参数为参数位置（1开始），第二个参数为参数的值
            preparedStatement.setString(1, "tom");
            // 执行sql，获得查询结果
            resultSet = preparedStatement.executeQuery();
            // 变量结果
            User user = new User();
            while (resultSet.next()) {
                int id = resultSet.getInt("uid");
                String username = resultSet.getString("username");
                // 封装结果
                user.setUid(id);
                user.setUsername(username);
            }
            System.out.println(user);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 释放资源
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
```

 **问题总结：** 

- 连接频繁创建、释放造成了资源浪费，影响系统性能
- Sql语句在代码中硬编码，不易维护
- 结果集封装硬编码，不易维护

 **解决思路** 
- 使用连接池
- 使用配置文件维护sql
- 封装结果集使用反射、内省

#### 自定义持久层框架思路

 **使用端（需要使用持久层的项目）：引入自定义持久层的jar包** 

主要提供2部分配置信息：数据库配置信息、sql配置信息（包含：sql语句、参数类型、返回类型）

使用配置文件来提供这2部分配置信息：
	
- sqlMapConfig.xml：存放数据库配置信息，存放mapper.xml全路径
- mapper.xml：存放sql配置信息


 **自定义持久层框架本身（持久层框架jar）：本质就是对JDBC代码进行封装** 

1. 加载配置文件：根据配置文件的路径，加载配置文件成字节输入流，存储在内存中

```
创建Resources类 方法：InputStream getResourceAsSteam(String path)
```
2. 创建2个javaBean（容器对象）：存放的就是对配置文件解析出来的内容

```
Configuration：核心配置类：存放sqlMapConfig.xml解析出来的内容
MapperStatement：映射配置类：存放mapper.xml解析出来的内容
```

3. 解析配置文件：dom4j
	
```
创建类：SqlSessionFactoryBUilder 方法：build(InputSteam in)
    第一：使用dom4j解析配置文件，将解析出来的内容封装到容器对象中
    第二：创建SqlSessionFactory对象，生产sqlSession：会话对象（工厂模式）
```

4. 创建SqlsessionFactory接口及实现DefaultSqlSessionFactory

```
方法openSeeion()：生产sqlSession
```

5. 创建SqlSession接口及实现类DefaultSession

```
定义对数据库的curd操作：selectList()、selectOne()、update()、delete()、insert()
```
	
6. 创建Excutor接口及实现类SimpleExcutor实现类

```
JDBC操作的执行过程：
	1、注册驱动，获取连接
    	2、获取Sql语句 然后转换sql语句，并对#{}里面的值进行解析存储
    	3、获取预处理对象： preparedStatement
    	4、设置参数
	5、执行sql
    	6、返回更新的条数
	
	prepareStatement(Configuration configuration, MappedStatement mappedStatement, Object... params)：准备预处理对象（1、2、3、4）
	query(Configuration configuration, MappedStatement mappedStatement, Object... params)：查询操作的执行（5、6）
	update(Configuration configuration, MappedStatement mappedStatement, Object... params):增、删、改操作的执行（5、6）
```

	

#### 自定义持久层框架测试 

com.ozdemo.test.IPersistenceTest

```Java
public void test() throws Exception {
        InputStream inputStream = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //查询
        System.out.println("------------------ 查询 -------------------");
        User user = new User();
        user.setUid(1);
        user.setUsername("张三");

        IUserDao iUserDao = sqlSession.getMapper(IUserDao.class);
        User resUser1 = iUserDao.findOne(user);
        System.out.println("查询结果：" + resUser1.toString());

        //新增
        System.out.println();
        System.out.println("------------------ 新增 -------------------");
        User user1 = new User();
        user1.setUsername("XX7");
        int res1 = iUserDao.insertOne(user1);
        System.out.println("添加成功条数：" + res1);

        //修改
        System.out.println();
        System.out.println("------------------ 修改 -------------------");
        User user2 = new User();
        user2.setUid(1);
        user2.setUsername("XX-update");
        int res2 = iUserDao.updateOne(user2);
        System.out.println("更新成功条数：" + res2);

        //删除
        System.out.println();
        System.out.println("------------------ 删除 -------------------");
        int res3 = iUserDao.deleteById(6);
        System.out.println("删除成功条数：" + res3);
    }
```

#### Mybatis框架核心
	Configuration：初始化基础配置，所有的配置信息都在Configuration中

	SqlSessionfactory：Sqlsession工厂
	Sqlsession：Mybatis的顶层api,表示数据库交互的会话，完成数据库的增删查改的一些功能

	Executor：Mybatis的执行器，是Mybatis调度的核心，负责sql语句的生成和查询缓存

	StatementHandler：封装了JDBC Statement 操作
	ParameterHandler：负责将传入的参数转化为JDBC Statment 的参数
	ResultSethandler：负责将JDBC返回的Resultset集对象封装成返回集合
	TypeHandler：负责将数据库的类型何实体类型之间的转化何映射

	MappedStatement：MappedStatement是select,update,delete,insert节点的封装

	SqlSource：负责根据用户传递的parameterObject 动态的生成一条sql语句
	Boundsql：表示动态生成的sql何对应的参数信息
	
#### Mybatis执行过程
<div align=center> <img src="https://images.gitee.com/uploads/images/2020/1120/110515_029a7396_797716.jpeg" width="300" />  <br>执行流程图</div>

#### Mybatis相关问题
1、Mybatis动态sql是做什么的？都有哪些动态sql？简述一下动态sql的执行原理？

- 动态sql是通过在xml文件中添加条件判断标签，从而动态判断拼接成sql语句；
- 动态sql标签包括有：if、choose（when，otherwise）、trim、 where、set、foreach
- 动态sql其实就是使用Ognl表达式，通过sql的参数对象计算出表达式的值，然后进行动态的判断拼接

2、Mybatis是否支持延迟加载？如果支持，它的实现原理是什么？	

- Mybatis支持延迟加载，只能在resultMap中使用，支持一对一和一对多的延迟加载，分别使用association和cllection标签
- 原理：通过CGLib创建目标对象的代理对象，当调用目标对象的方法时，调用目标对象的查询方法，从而实现使用时再查询，减轻了数据库的压力；即当需要关联查询时再进行数据查询

3、Mybatis都有哪些Executor执行器？它们之间的区别是什么？

- Mybatis有3种Executor执行器：SimpleExecutor、ReuseExecutor、BatchExecutor
- 区别：
    
```
SimpleExecutor：每次执行都会创建一个Statement对象
ReuseExecutor：每次执行都会缓存Statement对象，并以sql作为可以，如果存在就直接使用，不存在就创建
BatchExecutor：批处理执行器，仅支持update，先将所有sql都通过addBatch()方法添加到批处理中，然后通过executeBatch()方法统一执行
```

- 可在Mybatis配置文件中配置默认执行器类型，也可以在创建SqlSession时传入ExecutorType参数

4、简述下Mybatis的一级、二级缓存（分别从存储结构、范围、失效场景。三个方面来作答）？

- 一级缓存：Mybatis的一级缓存是SqlSession下的缓存，作用在Sqlsession中，当Sqlsession关闭或者查询的数据update时缓存失效；内部使用HashMap进行存储，以CacheKey对象做为key，当查询时会先去查询缓存中是否存在，如果存在直接返回或者查询数据库
- 二级缓存：二级缓存属于Mapper的缓存，多个SqlSession共用二级缓存，当同一个Mapper中的增删改时缓存失效；内部同样使用HashMap进行存储，同样使用CacheKey对象做为key；Mybatis中默认不开启二级缓存，可通过cacheEnabled来设置是否开启二级缓存；在分布式环境下可配置redis等来做为二级缓存

5、简述Mybatis的插件运行原理，以及如何编写一个插件？

- Mybatis的4大组件ParameterHandler、ResultSetHandler、StatementHandler、Executor提供了扩展插件机制；Mybatis使用动态代理，来为需要拦截的接口生产代理对象，以实现接口方法拦截功能；
- 实现Interceptor接口并重写intercept()方法，然后加上@Interceptors注解，指定需要拦截的方法，将自定义的插件配置到配置文件中

#### Mybatis基本应用

MyBatis是一个半自动的持久层框架，核心代码和sql分开，通过配置文件维护sql
Mybatis官网：[http://www.mybatis.org/mybatis-3/](http://www.mybatis.org/mybatis-3/)

 **MyBatis 开发步骤：** 
- 添加MyBatis相关依赖

```
<!--mybatis依赖-->
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>3.4.5</version>
</dependency>
<!--mysql驱动依赖-->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.6</version>
    <scope>runtime</scope>
</dependency>
```
- 创建user数据表
- 创建User对象

```
public class User {
 private int id;
 private String username;
 private String password;
}
```
- 编写映射文件UserMapper.xml

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.lagou.dao.IUserDao">
    <!--namespace : 名称空间：与id组成sql的唯一标识
        resultType： 表明返回值类型-->

    <!--抽取sql片段-->
    <sql id="selectUser">
         select * from user
    </sql>


    <!--查询用户-->
    <select id="findAll" resultType="uSeR">
        <include refid="selectUser"></include>
    </select>

    <!--添加用户-->
    <!--parameterType：参数类型-->
    <insert id="saveUser" parameterType="user">
        insert into user values(#{id},#{username})
    </insert>

    <!--修改-->
    <update id="updateUser" parameterType="user">
        update user set username = #{username} where id = #{id}
    </update>

    <!--删除-->
    <delete id="deleteUser" parameterType="int">
        delete from user where id = #{abc}
    </delete>

    <!--多条件组合查询：演示if-->
    <select id="findByCondition" parameterType="user" resultType="user">
        <include refid="selectUser"></include>
        <where>
            <if test="id !=null">
                and id = #{id}
            </if>
            <if test="username !=null">
                and username = #{username}
            </if>
        </where>

    </select>

    <!--多值查询：演示foreach-->
    <select id="findByIds" parameterType="list" resultType="user">
        <include refid="selectUser"></include>
        <where>
            <foreach collection="array" open="id in (" close=")" item="id" separator=",">
                #{id}
            </foreach>
        </where>

    </select>
</mapper>
```

- 创建UserDao
```
public interface IUserDao {
    //查询所有用户
    public List<User> findAll() throws IOException;

    //多条件组合查询：演示if
    public List<User> findByCondition(User user);
    
    //多值查询：演示foreach
    public List<User> findByIds(int[] ids);
}
```

- 创建外部配置文件 jdbc.properties

```
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/ozdemo?characterEncoding=utf-8
jdbc.username=root
jdbc.password=123456
```

- 编写配置文件SqlMapConfig.xml

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">

<configuration>
    <!--加载外部的properties文件-->
    <properties resource="jdbc.properties"></properties>
    <!--给实体类的全限定类名给别名-->
    <typeAliases>
        <!--给单独的实体起别名-->
        <!--  <typeAlias type="com.lagou.pojo.User" alias="user"></typeAlias>-->
        <!--批量起别名：该包下所有的类的本身的类名：别名还不区分大小写-->
        <package name="com.lagou.pojo"/>
    </typeAliases>

    <!--environments:运行环境-->
    <environments default="development">
        <environment id="development">
            <!--当前事务交由JDBC进行管理-->
            <transactionManager type="JDBC"></transactionManager>
            <!--当前使用mybatis提供的连接池-->
            <dataSource type="POOLED">
                <property name="driver" value="${jdbc.driver}"/>
                <property name="url" value="${jdbc.url}"/>
                <property name="username" value="${jdbc.username}"/>
                <property name="password" value="${jdbc.password}"/>
            </dataSource>
        </environment>
    </environments>
    <!--引入映射配置文件-->
    <mappers>
        <mapper resource="UserMapper.xml"></mapper>
    </mappers>
</configuration>
```

- 测试

```
public void test() throws IOException {
    InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
    SqlSession sqlSession = sqlSessionFactory.openSession();

    IUserDao mapper = sqlSession.getMapper(IUserDao.class);
    List<User> all = mapper.findAll();
    for (User user : all) {
       System.out.println(user);
    }
}
```

#### Mybatis源码分析
 **源码分析-初始化 （解析配置文件，创建了sqlSessionFactory工厂）** 

1、初始化开始
```
InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
//解析了配置文件，并创建了sqlSessionFactory工厂
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
```
2、org.apache.ibatis.session.SqlSessionFactoryBuilder#build(java.io.InputStream, java.lang.String, java.util.Properties)
```
public SqlSessionFactory build(InputStream inputStream, String environment, Properties properties) {
    try {
      //解析Mybatis配置文件的类
      XMLConfigBuilder parser = new XMLConfigBuilder(inputStream, environment, properties);
      //parser.parse()解析配置文件，返回Configuration对象
      return build(parser.parse());
    } catch (Exception e) {
      throw ExceptionFactory.wrapException("Error building SqlSession.", e);
    } finally {
      ErrorContext.instance().reset();
      try {
        inputStream.close();
      } catch (IOException e) {
        // Intentionally ignore. Prefer previous error.
      }
    }
  }
```
3、org.apache.ibatis.builder.xml.XMLConfigBuilder#parse

```
/*
* 解析配置文件并返回Configuration
**/
public Configuration parse() {
    //若已解析就抛出异常
    if (parsed) {
      throw new BuilderException("Each XMLConfigBuilder can only be used once.");
    }
    //标记为已解析
    parsed = true;
    //解析 configuration 节点
    parseConfiguration(parser.evalNode("/configuration"));
    return configuration;
  }

/*
* 解析标签
**/
  private void parseConfiguration(XNode root) {
    try {
      //issue #117 read properties first
      propertiesElement(root.evalNode("properties"));
      Properties settings = settingsAsProperties(root.evalNode("settings"));
      loadCustomVfs(settings);
      typeAliasesElement(root.evalNode("typeAliases"));
      pluginElement(root.evalNode("plugins"));
      objectFactoryElement(root.evalNode("objectFactory"));
      objectWrapperFactoryElement(root.evalNode("objectWrapperFactory"));
      reflectorFactoryElement(root.evalNode("reflectorFactory"));
      settingsElement(settings);
      // read it after objectFactory and objectWrapperFactory issue #631
      environmentsElement(root.evalNode("environments"));
      databaseIdProviderElement(root.evalNode("databaseIdProvider"));
      typeHandlerElement(root.evalNode("typeHandlers"));
      // 解析Mapper配置文件
      mapperElement(root.evalNode("mappers"));
    } catch (Exception e) {
      throw new BuilderException("Error parsing SQL Mapper Configuration. Cause: " + e, e);
    }
  }
```
4、org.apache.ibatis.builder.xml.XMLConfigBuilder#mapperElement 解析Mapper配置文件

5、org.apache.ibatis.builder.xml.XMLMapperBuilder#parse 解析 mapper节点

6、org.apache.ibatis.builder.xml.XMLStatementBuilder#parseStatementNode 解析"select|insert|update|delete"标签 生成MappedStatement，并存入Configuration中

7、解析完成后回到第2步，创建SqlSessionFactory

```
public SqlSessionFactory build(Configuration config) {
    return new DefaultSqlSessionFactory(config);
  }
```

 **源码分析--SQL执行流程**
 
1、生成sqlSession对象

```
//3.生产sqlSession
SqlSession sqlSession = sqlSessionFactory.openSession();
//4.sqlSession调用方法：查询所有selectList  查询单个：selectOne 添加：insert  修改：update 删除：delete
List<User> users = sqlSession.selectList("user.findAll");
```

2、org.apache.ibatis.session.defaults.DefaultSqlSessionFactory#openSession() 进入openSession方法

```
//进入openSession
 @Override
  public SqlSession openSession() {
    return openSessionFromDataSource(configuration.getDefaultExecutorType(), null, false);
  }
```
3、org.apache.ibatis.session.defaults.DefaultSqlSessionFactory#openSessionFromDataSource

```
/*
* ExecutorType execType：Executor类型
* TransactionIsolationLevel level： 事务隔离级别
* autoCommit：是否开启事务
**/
private SqlSession openSessionFromDataSource(ExecutorType execType, TransactionIsolationLevel level, boolean autoCommit) {
    Transaction tx = null;
    try {
      final Environment environment = configuration.getEnvironment();
      final TransactionFactory transactionFactory = getTransactionFactoryFromEnvironment(environment);
      tx = transactionFactory.newTransaction(environment.getDataSource(), level, autoCommit);
        //创建Executor
      final Executor executor = configuration.newExecutor(tx, execType);
        //创建SqlSession
      return new DefaultSqlSession(configuration, executor, autoCommit);
    } catch (Exception e) {
      closeTransaction(tx); // may have fetched a connection so lets call close()
      throw ExceptionFactory.wrapException("Error opening session.  Cause: " + e, e);
    } finally {
      ErrorContext.instance().reset();
    }
  }
```
4、org.apache.ibatis.session.defaults.DefaultSqlSession#selectList(java.lang.String, java.lang.Object, org.apache.ibatis.session.RowBounds) 此方法是直接调用

```
@Override
  public <E> List<E> selectList(String statement, Object parameter, RowBounds rowBounds) {
    try {
        //获取初始化的 MappedStatement 对象
      MappedStatement ms = configuration.getMappedStatement(statement);
        //调用excutor处理，rowBounds处理分页逻辑，wrapCollection(parameter)装饰集合或者数组参数
      return executor.query(ms, wrapCollection(parameter), rowBounds, Executor.NO_RESULT_HANDLER);
    } catch (Exception e) {
      throw ExceptionFactory.wrapException("Error querying database.  Cause: " + e, e);
    } finally {
      ErrorContext.instance().reset();
    }
  }
```
5、org.apache.ibatis.executor.BaseExecutor#query(org.apache.ibatis.mapping.MappedStatement, java.lang.Object, org.apache.ibatis.session.RowBounds, org.apache.ibatis.session.ResultHandler)  executor执行逻辑

```
@Override
  public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException {
    //根据参数动态生成SQL语句，返回BoundSql
    BoundSql boundSql = ms.getBoundSql(parameter);
    //创建缓存key
    CacheKey key = createCacheKey(ms, parameter, rowBounds, boundSql);
    //进入重载方法 如果缓存中没有就查库
    return query(ms, parameter, rowBounds, resultHandler, key, boundSql);
 }
```

6、org.apache.ibatis.executor.BaseExecutor#queryFromDatabase 查询数据库

```
private <E> List<E> queryFromDatabase(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
    List<E> list;
    localCache.putObject(key, EXECUTION_PLACEHOLDER);
    try {
        //查询数据
      list = doQuery(ms, parameter, rowBounds, resultHandler, boundSql);
    } finally {
      localCache.removeObject(key);
    }
    //缓存数据
    localCache.putObject(key, list);
    if (ms.getStatementType() == StatementType.CALLABLE) {
      localOutputParameterCache.putObject(key, parameter);
    }
    return list;
  }
```
7、org.apache.ibatis.executor.SimpleExecutor#doQuery 执行查询逻辑

```
@Override
  public <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
    Statement stmt = null;
    try {
      Configuration configuration = ms.getConfiguration();
        //创建StatementHandler来执行查询
      StatementHandler handler = configuration.newStatementHandler(wrapper, ms, parameter, rowBounds, resultHandler, boundSql);
        //创建JDBC中的Statement
      stmt = prepareStatement(handler, ms.getStatementLog());
        //StatementHandler执行查询
      return handler.<E>query(stmt, resultHandler);
    } finally {
      closeStatement(stmt);
    }
  }

/**
*创建 jdbc中的 Statement
**/
private Statement prepareStatement(StatementHandler handler, Log statementLog) throws SQLException {
    Statement stmt;
    //创建数据库连接
    Connection connection = getConnection(statementLog);
    stmt = handler.prepare(connection, transaction.getTimeout());
    handler.parameterize(stmt);
    return stmt;
  }

/**
*创建数据库连接
**/
protected Connection getConnection(Log statementLog) throws SQLException {
    //获取数据库连接
    Connection connection = transaction.getConnection();
    if (statementLog.isDebugEnabled()) {
      return ConnectionLogger.newInstance(connection, statementLog, queryStack);
    } else {
      return connection;
    }
  }

protected void openConnection() throws SQLException {
    if (log.isDebugEnabled()) {
      log.debug("Opening JDBC Connection");
    }
    //从连接池中获取连接
    connection = dataSource.getConnection();
    if (level != null) {
      connection.setTransactionIsolation(level.getLevel());
    }
    setDesiredAutoCommit(autoCommmit);
  }
```













